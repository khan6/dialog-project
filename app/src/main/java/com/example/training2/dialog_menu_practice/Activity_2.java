package com.example.training2.dialog_menu_practice;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Activity_2 extends AppCompatActivity {

    private TextView dPick2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        dPick2 = findViewById(R.id.dialog_butt2);

        dPick2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final Dialog dialog2 = new Dialog(Activity_2.this);
                dialog2.setContentView(R.layout.custom2);
                dialog2.setTitle("Title...");

                dialog2.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog2.dismiss();
                    }
                });

                dialog2.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog2.dismiss();
                    }
                });


                dialog2.show();
            }
        });


    }

}
