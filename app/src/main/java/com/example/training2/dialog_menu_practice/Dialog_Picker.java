package com.example.training2.dialog_menu_practice;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

  public class Dialog_Picker extends AppCompatActivity {

    private TextView dPick;
    private Button act_2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog__picker);

        dPick = findViewById(R.id.dialog_butt);


        dPick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(Dialog_Picker.this);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("Title...");

                dialog.findViewById(R.id.active_1).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.findViewById(R.id.active_2).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Dialog_Picker.this,Activity_2.class);
                        startActivity(i);
                    }
                });

                dialog.findViewById(R.id.active_3).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Dialog_Picker.this,Activity_3.class);
                        startActivity(i);
                    }
                });

                dialog.findViewById(R.id.active_4).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Dialog_Picker.this,Activity_4.class);
                        startActivity(i);
                    }
                });
                // set the custom dialog components - text, image
                dialog.show();

                }
        });

    }



}
